//Sanitize the data from the api it will remove the comma and convert strings into number

export default function toNum(str) {
    //convert the string to array to gain access to array methods by using the spread operator
    const arr = [...str]
    //if we console this, console.log(arr) ["2", "5", ", "2", "2", "2",",".....]
    const filteredArr = arr.filter(element => element !== ",")
    //we filter out the comma in the string
    return parseInt(filteredArr.reduce((x, y) => x + y))
}
    // reduce the filtered array back  to a single string without the commas

 /*    reduce()
    on the 1st iteration:

    x is the 1st item in the Array
    y is the 2nd item

    "2" + "5" = "25"
    "25" + "2" = "252" */

    //reduce to a single string. this string will then be parsed as integer


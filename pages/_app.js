import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {Container} from 'react-bootstrap'
import NavBar from '../components/NavBar'

function MyApp({ Component, pageProps }) {
  return (
  <React.Fragment>
    <NavBar/>
    <Container className='my-5'>
      <Component {...pageProps} />
    </Container>
  </React.Fragment>
  )
}

export default MyApp

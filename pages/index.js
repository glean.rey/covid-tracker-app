import React from 'react';
import Head from 'next/head';
import {Jumbotron} from 'react-bootstrap';
import toNum from '../helper/toNum';

export default function Home({globalTotal}) {
  console.log(globalTotal)
  return (
    <React.Fragment>
      <Jumbotron>
        <h1 className='text-center'>Corona Virus Tracker</h1>
      </Jumbotron>
    </React.Fragment>
  )
}

export async function getStaticProps() {
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })
  const data = await res.json()
  const countriesStats = data.countries_stat
  //countries_stats is a property of data and contains an array of object. Each object has the covid statistics of a country

    let total = 0 //this variable will be where our cases will be accumulated into 
    // we use forEach to accumulate the number of cases in each country
    countriesStats.forEach(country => {
      total += toNum(country.cases) // this is where we used toNum
    })

    /* 
    0 += 298476664
    */

    const globalTotal = {
      cases: total
    }

    return {
      props: {
        globalTotal
      }
    }


} 


import ListGroup from 'react-bootstrap/ListGroup'
import Link from 'next/link'
import Head from 'next/head'
import React from 'react'

function index({data}) {
    const countryList = data.countries_stat.map((country) => {
        return (
            <ListGroup.Item key={country.country_name}>
                <Link href={`/covid/countries/${country.country_name}`}>{country.country_name}</Link>
            </ListGroup.Item>
        )
    })
    return (
        <React.Fragment>
            <Head>
                <title>COVID-19 Infected Countries</title>
            </Head>
            <ListGroup>
                {countryList}
            </ListGroup>
        </React.Fragment>
    )
}

export async function getStaticProps() {
    const response = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
        'method': 'GET',
        'headers': {
            'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
            'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
        }
    })

    const casesByCountry = await response.json();

    return {
        props: {
            data: casesByCountry
        }
    }
}

export default index

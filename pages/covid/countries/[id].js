import React, {useState} from 'react'
import {Doughnut} from 'react-chartjs-2'
import toNum from '../../../helper/toNum';
import DoughnutChart from '../../../components/DoughnutChart';

export async function getStaticPaths() {
    const response = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
        'method': 'GET',
        'headers': {
            'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
            'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
        }
    })

    const data = await response.json()
    const paths = data.countries_stat.map(country => (
        {
            params: {id: country.country_name}
        }
    ))

    return {
        paths: paths,
        fallback: false
    }
}

export async function getStaticProps({params}) {
    const response = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
        'method': 'GET',
        'headers': {
            'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
            'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
        }
    })

    const casesByCountry = await response.json();
    const country = casesByCountry.countries_stat.find((country) => {
        console.log(country);
        return country.country_name === params.id
    })
    console.log(country)
    return {
        props: {
            country: country
        }
    }
}

export default function country({country}) {
    
    return (
        <React.Fragment>
            <h1>Country: {country.country_name}</h1>
            <DoughnutChart
                criticals={toNum(country.serious_critical)}
                deaths={toNum(country.deaths)}
                recoveries={toNum(country.total_recovered)}
                />
        </React.Fragment>
    )
}

import React from 'react'
import toNum from '../../helper/toNum'
import {Doughnut} from 'react-chartjs-2'

function top({ data }) {
    
    const countriesStat = data.countries_stat.map((country) => {
        return {
            name: country.country_name,
            cases: toNum(country.cases)
        }
    })
    
    countriesStat.sort((a, b) => b.cases - a.cases)
    
   /*  {
        if (a.cases < b.cases) {
            return 1
            swap 100 and 800
            800, 100
        } else if (a.cases > b.cases) {
            return -1
            800 > 100
            do nothing (nothing change)
        } else {
            return 0
            do nothing
            100 === 100
        }
    }) */

    const doughnutChartSettings = {
        datasets: [{
            data: [
                countriesStat[0].cases,
                countriesStat[1].cases,  
                countriesStat[2].cases, 
                countriesStat[3].cases, 
                countriesStat[4].cases, 
                countriesStat[5].cases, 
                countriesStat[6].cases, 
                countriesStat[7].cases, 
                countriesStat[8].cases, 
                countriesStat[9].cases
            ],
            backgroundColor: [
                "#696969", 
                "#bada55", 
                "#7fe5f0", 
                "#ff0000", 
                "#ff80ed", 
                "#407294", 
                "#cbcba9", 
                "#420420", 
                "#133337", 
                "#065535" 
            ],
        }],
        labels: [
            countriesStat[0].name,
            countriesStat[1].name,  
            countriesStat[2].name, 
            countriesStat[3].name, 
            countriesStat[4].name, 
            countriesStat[5].name, 
            countriesStat[6].name, 
            countriesStat[7].name, 
            countriesStat[8].name, 
            countriesStat[9].name
        ]
    }

    console.log(countriesStat);

    return (
        <React.Fragment>
            <h1>10 Countries with the Highest Number of Cases</h1>
            <Doughnut data={doughnutChartSettings} redraw={false}/>
        </React.Fragment>
    )
}
export async function getStaticProps() {
    const response = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
        'method': 'GET',
        'headers': {
            'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
            'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
        }
    })

    const casesByCountry = await response.json();

    return {
        props: {
            data: casesByCountry
        }
    }
}


export default top

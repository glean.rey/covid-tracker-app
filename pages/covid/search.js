import React, {useState} from 'react';
import toNum from '../../helper/toNum';
import {Form, Button} from 'react-bootstrap';
import DoughnutChart from '../../components/DoughnutChart';

export default function Search({data}) {
    const countriesStats = data.countries_stat 
    const [targetCountry, setTargetCountry] = useState('')
    const [name, setName] = useState('')
    const [criticals, setCriticals] = useState(0)
    const [deaths, setDeaths] = useState(0)
    const [recoveries, setRecoveries] = useState(0)

    function search(e) {
        e.preventDefault()

        const match = countriesStats.find(country => country.country_name.toLowerCase()===targetCountry.toLowerCase())
        console.log(match)
        //set this to use for the donut chart
        setName(match.country_name)
        setCriticals(toNum(match.serious_critical))
        setDeaths(toNum(match.deaths))
        setRecoveries(toNum(match.total_recovered))
    }
    
    return(
        <React.Fragment>
            <Form onSubmit={e => search(e)}>
                <Form.Group controlId="country">
                    <Form.Label>Country</Form.Label>
                    <Form.Control 
                    type="text"
                    placeholder= "Search for Country"
                    value={targetCountry}
                    onChange={e => setTargetCountry(e.target.value)}/>
                    <Form.Text className="text-muted">
                        Get Covid-19 stats of searched for country
                    </Form.Text>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
            <h1 className="my-3">Country: {name}</h1>
            <DoughnutChart
                criticals={criticals}
                deaths={deaths}
                recoveries={recoveries}
                />
        </React.Fragment>
    )
}

export async function getStaticProps() {
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "method": "GET",
      "headers": {
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
      }
    })
    const data = await res.json()
    
      return {
        props: {
          data
        }
      }
  
  
  } 
  
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';

export default function navBar() {
    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand"> Covid-19 Tracker </a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto justify-content-end w-100">
                    <Link href="/covid/countries">
                        <a className="navbar-link"> Infected Countries </a>
                    </Link>
                    <Link href="/covid/search">
                        <a className="navbar-link ml-3"> Find a Country </a>
                    </Link>
                    <Link href="/covid/top">
                        <a className="navbar-link ml-3"> Top Countries </a>
                    </Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    ) 
} 
import {Doughnut} from 'react-chartjs-2';

export default function DoughnutChart({criticals, deaths, recoveries}) {
 console.log(criticals, deaths, recoveries)
    return (
        <Doughnut data={{
            datasets: [{
                data: [criticals, deaths, recoveries],
                backgroundColor: ["yellow", "red", "green"]
            }],
            labels: [
                'Criticals',
                'Deaths',
                'Recoveries',
            ]
        }} redraw={true}/>
    )

}